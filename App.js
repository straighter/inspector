import React, { Component } from 'react';
import { Provider, } from 'react-redux';
import { PersistGate } from 'redux-persist/lib/integration/react';
import { YellowBox, StatusBar, View } from 'react-native';
import Config from 'react-native-config';

import { store, persistor } from './stores'
import Routes from './routes.js';
import MapboxGL from '@mapbox/react-native-mapbox-gl';

// Set Mapbox token
MapboxGL.setAccessToken(Config.MAPBOX_TOKEN);

// Render the app's routes after state has been persisted
export default class App extends Component {
  state = {
    isReady: false
  }

  async componentDidMount() {
    this.setState({ isReady: true });
  }

  render() {
    return <Provider store={store}>
      <PersistGate loading={<View />} persistor={persistor}>
        <View style={{ flex: 1 }}>
          <StatusBar  barStyle="light-content" />
          <Routes />
        </View>
      </PersistGate>
    </Provider>;
  }
}

YellowBox.ignoreWarnings([
  'Warning: componentWillMount is deprecated',
  'Warning: componentWillReceiveProps is deprecated',
  'Warning: isMounted(...) is deprecated'
]);