import { authFetch } from './helpers';
import Config from 'react-native-config'


// Create asset after getting a prediction and uploading the image to S3
export async function createAssetFull(asset, accessToken, refreshToken, refresh) {
    try { 
        const prediction = await getPrediction(asset.uri, accessToken, refreshToken, refresh);
        
        asset.prediction = prediction;

        const uri = await createImage(asset.uri, accessToken, refreshToken, refresh);

        asset.uri = uri;

        const actualAsset = await createAsset(asset, accessToken, refreshToken, refresh);
        return actualAsset;
    } catch (e) {
        throw e;
    }
}

// Create an image on S3
export function createImage(uri, accessToken, refreshToken, refresh) {
    const formData = new FormData();
    
    formData.append('file', {
        uri,
        name: uri.split('/')[uri.split('/').length - 1],
        type: 'image/jpeg'
    });

    const options = {
        headers: createHeaders(accessToken),
        method: 'post',
        body: formData
    };

    options.headers.append('Content-Type', 'multipart/form-data');

    try {
        return authFetch(`https://moaa-dev.herokuapp.com/v1/assets/picture`, options, refreshToken, refresh)
            .then(data => data.json())
            .then(data => data.uri)
    } catch (e) {
        throw e
    }
}

// Create an asset
export async function createAsset(asset, accessToken, refreshToken, refresh) {
    const options = {
        headers: createHeaders(accessToken),
        method: 'post',
        body: JSON.stringify({...asset})
    };

    options.headers.append('Content-Type', 'application/json')

    try {
        return await authFetch(`${Config.BASE_URI}/assets`, options, refreshToken, refresh)
            .then(data => data.json());
    } catch (e) {
        throw e;
    }
}

// Get a prediction by an image
export function getPrediction(uri, accessToken, refreshToken, refresh) {
    const formData = new FormData();
    formData.append('file', {
        uri,
        name: uri.split('/')[uri.split('/').length - 1],
        type: 'image/jpeg'
    });

    const options = {
        headers: createHeaders(accessToken),
        method: 'post',
        body: formData
    };

    options.headers.append('Content-Type', 'multipart/form-data');

    try {
        return authFetch(`${Config.BASE_URI}/predict`, options, refreshToken, refresh)
            .then(data => data.json())
    } catch (e) {
        throw e
    }
}

// Get an asset by id.
export async function fetchAssetById(id, accessToken, refreshToken, refresh) {
    try {
        return await authFetch(`${Config.BASE_URI}/assets/${id}`, accessToken, refreshToken, refresh)    
    } catch (e) {
        console.log(e);
    }
}

// Create base headers
function createHeaders (accessToken) {
    const headers = new Headers();
    headers.append('Accept', 'text/html');
    headers.append('Authorization', `Bearer ${accessToken}`);
    return headers;
}

// Create a review for an asset by id
export async function createAssetReview(id, review, accessToken, refreshToken, onChange) {
    const options = {
        headers: createHeaders(accessToken),
        method: 'put',
        body: JSON.stringify({...review})
    };

    options.headers.append('Content-Type', 'application/json')

    return authFetch(`${Config.BASE_URI}/assets/${id}/review`, options, refreshToken, onChange)
        .then(data => data.json()) 
        .then(data => deserializeAsset(data))
}

// Get all assets
export async function fetchAssets(accessToken, refreshToken, onChange) {
    const options = {
        headers: createHeaders(accessToken)
    };

    try {
        return await authFetch(`${Config.BASE_URI}/assets`, options, refreshToken, onChange)    
          .then(data => data.json())
          .then(data => deserializeAssets(data))
    } catch (e) {
        console.log(e);
    }
}

// Deserialize an asset response to usable JSON
const deserializeAsset = asset => {
    return    ({
        id: asset._id.$oid,
        author: asset.author,
        location: asset.location,
        prediction: asset.prediction,
        uri: asset.uri,
        reviews: asset.reviews
    })
};

const deserializeAssets = assets => assets.map(deserializeAsset);
