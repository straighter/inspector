import Config from 'react-native-config'

// Refresh the user using a refresh token.
export async function refresh(refreshToken) {
  const headers = new Headers();
  headers.append('Content-Type', 'application/json');
  headers.append('Authorization', `Bearer ${refreshToken}`);
  
  const response = await fetch(`${Config.BASE_URI}/users/refresh`, {
    method: 'post',
    headers
  });

  const data = await response.json();

  if (response.status >= 400) {
    console.log(response.status, data);
    throw Error(data)
  }

  return {
    id: data.user_id,
    username: data.username,
    accessToken: data.access_token
  };
}

// Login the user
export async function login(username, password) {
  const headers = new Headers();
  headers.append('Content-Type', 'application/json');
  const response = await fetch(`${Config.BASE_URI}/users/login`, {
    method: 'post',
    body: JSON.stringify({ username, password }),
    headers
  });

  const data = await response.json();

  if (response.status >= 400) {
    console.log('ERROR | LOGIN:', JSON.stringify({ username, password }));
    throw Error(data);
  }

  return {
    id: data.user_id,
    username: data.username,
    accessToken: data.access_token,
    refreshToken: data.refresh_token
  };
}
