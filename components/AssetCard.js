import React from 'react';
import { ImageBackground } from 'react-native';
import { Card, CardItem, View, Content, Body, Left, Right, Icon, Button, Text } from 'native-base';
import MapView, { Marker } from 'react-native-maps';
import Emoji from 'react-native-emoji';

import PredictionBadge from './PredictionBadge';
import { reaction } from 'mobx';
import MapboxGL from '@mapbox/react-native-mapbox-gl';
import idx from 'idx';

class AssetCard extends React.Component {
    render() {
        return (
            <Card style={style.card}>
                <CardItem>
                    <Left>
                        <Body>
                            <Text style={style.cardTitle}>Asset {this.props.id}</Text>
                            <Text note>door {this.props.author}</Text>
                        </Body>
                    </Left>
                </CardItem>
                <CardItem cardBody>
                    <ImageBackground style={style.content} imageStyle={{ flex: 1, width: undefined, height: undefined }} source={{ uri: this.props.uri }}>
                        <View style={style.predictionOverlayLeft}> 
                            <PredictionBadge value={this.props.reviewAverage} />
                        </View>
                        <View style={style.predictionOverlayRight}> 
                            <PredictionBadge value={idx(this.props.prediction, _ => _.value)} isRobot />
                        </View>
                    </ImageBackground>
                </CardItem>
                <CardItem cardBody>
                    <View style={{flex: 1}}>
                        {this.props.location && 
                            <MapboxGL.MapView
                                style={{ height: 150, width: undefined }}
                                centerCoordinate={this.props.location}
                                zoomLevel={13}
                                animated={true}
                            >
                                <MapboxGL.PointAnnotation
                                    key='assetAnnotation'
                                    id='assetAnnotation'
                                    coordinate={this.props.location.reverse()}
                                />
                            </MapboxGL.MapView>
                        }
                    </View>
                </CardItem>
            </Card>
        )
    }
}


const style = {
    card: { elevation: 1, height: undefined, width: '100%' },
    cardTitle: { fontWeight: 'bold' },
    locationText: { flex: 1, textAlign: 'right' },
    timesReviewed: { flexDirection: 'row', justifyContent: 'center', alignItems: 'center' },
    timesReviewedText: { fontSize: 16, marginLeft: 4 },
    content: { flex: 1, height: 450 },
    predictionOverlayLeft: { position: 'absolute', bottom: 15, left: 15 },
    predictionOverlayRight: { position: 'absolute', bottom: 15, right: 15 }
}

export default AssetCard;