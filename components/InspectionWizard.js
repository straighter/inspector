import React from 'react';
import { View, Button, Icon, Text, Footer } from 'native-base';

import ReviewButton from '../components/ReviewButton';

// Component that handles the inspection, renders all buttons and hooks up the right functions to each button
export default props => {
    return <Footer style={style.inspectionBar}>
        {props.children}
        <View style={[style.rowCentered, { flex: 1 }]}>
            <View style={style.rowCentered}>
                {[1, 2, 3, 4, 5, 6, 0].reverse().map(v => <ReviewButton key={`review_${v}`} onPress={props.setValue} active={props.reviewValue === v} value={v}/>)}
            </View>
            <Button rounded success disabled={props.reviewValue === null || props.reviewValue == undefined} onPress={props.submitValue}><Icon name="checkmark" /></Button>
        </View>
    </Footer>
}

const style = {
    inspectionBar: {
        backgroundColor: '#FFFFFF',
        alignItems: 'center',
        justifyContent: 'center'
    },
    rowCentered: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center'
    }
}