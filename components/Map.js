import React from 'react';
import MapBox from '@mapbox/react-native-mapbox-gl';
import { View, Text } from 'native-base';

export default props => <View style={style.container}>
    <MapBox.MapView
      style={style.map}
      centerCoordinate={props.center && props.center.length === 2 ? [...props.center].reverse() : null}
      zoomLevel={props.zoom || 15}
      animated={true}
      showUserLocation={props.showUserLocation}
    >
      {props.center ? props.children : null}
    </MapBox.MapView>

    {!props.center && <View style={style.mapOverlay}><Text style={style.mapOverlayText}>Geen locatie</Text></View>}
</View>

const style = {
  container: { flex: 1, justifyContent: 'center', alignItems: 'center', position: 'relative'},
  map: { position: 'absolute', top: 0, left: 0, bottom: 0, right: 0 },
  mapOverlay: { position: 'absolute', top: 0, left: 0, bottom: 0, right: 0, justifyContent: 'center', alignItems: 'center', backgroundColor: 'rgba(0, 0, 0, 0.3)' },
  mapOverlayText: { color: '#FFFFFF' }
}