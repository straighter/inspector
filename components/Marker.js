import React from 'react';
import MapBox from '@mapbox/react-native-mapbox-gl';
import { View } from 'native-base';
import v4 from 'uuid/v4';

export default props => {
  return <MapBox.PointAnnotation
    key={props.title || ''}
    description={props.description || ''}
    id={v4()}
    coordinate={[...props.coordinate].reverse()}
  />
};