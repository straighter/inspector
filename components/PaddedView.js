import React from 'react';
import { View } from 'native-base';

export default (props) => <View style={[
    { margin: 8 },
    props.full ? { margin: 0 } : {},
    props.style
]}>{props.children}</View>