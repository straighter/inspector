import React from 'react';
import { StyleSheet } from 'react-native';
import { Button, Text, View, Icon } from 'native-base';

const ReviewButton = props => <Button 
    style={[style.button, { backgroundColor: props.active ? '#64b5f6' : 'transparent'}]} 
    transparent={!props.active}
    rounded={props.active}
    light={props.active}
    onPress={() => props.onPress(props.value)}>
    {props.value === 0 ? <Icon style={[{ color: props.active ? '#FFFFFF' : '#000000' }]} name="close" /> : <Text style={[style.buttonText, { color: props.active ? '#FFFFFF' : '#000000' }]}>{props.value}</Text>
}
</Button>


const style = StyleSheet.create({
    button: { padding: 2, marginLeft: 2, marginRight: 2 },
    buttonText: { fontWeight: 'bold', fontSize: 16 },
    triangle: {
        width: 40,
        height: 40,
        backgroundColor: 'transparent',
        borderStyle: 'solid',
        borderTopWidth: 20,
        borderRightWidth: 20,
        borderBottomWidth: 0,
        borderLeftWidth: 20,
        borderTopColor: 'red',
        borderRightColor: 'transparent',
        borderBottomColor: 'transparent',
        borderLeftColor: 'transparent',
   }
})

export default ReviewButton;