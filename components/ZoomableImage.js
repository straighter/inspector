import React from 'react';
import PhotoView from 'react-native-photo-view';
import { View } from 'native-base';

export default props =>  <View style={style.container}>
  <PhotoView 
    source={props.source}
    style={style.canvas}
    maximumZoomScale={5}
    androidScaleType="fitCenter"
    resizeMode="cover"
  />
  {props.children}
</View>

const style = {
  container: { flex: 1, position: 'relative' },
  canvas: { position: 'absolute', top: 0, left: 0, bottom: 0, right: 0, zIndex: 1 }
}
