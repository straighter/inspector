import { combineReducers } from 'redux'
import {
  FETCH_ASSETS_SUCCESS,
  FETCH_ASSETS,
  FETCH_ASSETS_FAILURE,
  CREATE_ASSET_REVIEW,
  CREATE_ASSET_REVIEW_FAILURE,
  CREATE_ASSET_REVIEW_SUCCESS,
  CREATE_ASSET_SUCCESS,
  CREATE_ASSET_FAILURE
} from '../actions/asset';

// Handle isFetching assets state
const isFetching = (state = false, action) => {
  switch(action.type) {
    case FETCH_ASSETS: {
      return true;
    }
    default:
    case FETCH_ASSETS_SUCCESS:
    case FETCH_ASSETS_FAILURE: {
      return false;
    }
  }
}

// Handle fetching error assets state
const fetchingError = (state = null, action) => {
  switch (action.type) {
    case FETCH_ASSETS_FAILURE: {
      return action.payload.error;
    }
    default:
    case FETCH_ASSETS:
    case FETCH_ASSETS_SUCCESS: {
      return null;
    }
  }
}

// Handle assets state
const assets = (state = [], action) => {
  switch(action.type) {
    // When a new asset review is created, add the review to the asset in state
    case CREATE_ASSET_REVIEW: {
      return state.map(item => {
        if (item.id == action.payload.id) {
          const copyItem = {...item};
          copyItem.reviews = [];
          return copyItem;
        }
        return item;
      });
    }
    case FETCH_ASSETS_SUCCESS: {
      return [
        ...state,
        ...action.payload.assets
      ];
    }
    default: {
      return state;
    }
  }
}

export default combineReducers({
  fetchingError,
  isFetching,
  assets,
})