import { combineReducers } from 'redux'
import {
  LOGIN_USER_SUCCESS,
  LOGIN_USER,
  LOGIN_USER_FAILURE,
  REFRESH_USER,
  REFRESH_USER_SUCCESS,
  REFRESH_USER_FAILURE,
  SET_USER_AUTH
} from '../actions/user';

// Handle isLoggingIn auth state
const isLoggingIn = (state = null, action) => {
  switch(action.type) {
    case LOGIN_USER: {
      return action.payload.active;
    }
    default: {
      return false;
    }
  }
}

// Handle login erro auth state
const loginError = (state = null, action) => {
  switch (action.type) {
    case LOGIN_USER_FAILURE: {
      return action.payload.error;
    }
    default:
    case LOGIN_USER_SUCCESS: {
      return null;
    }
  }
}

// Handle user state
const user = (state = {
  id: null,
  username: null,
  accessToken: null,
  refreshToken: null
}, action) => {
  switch(action.type) {
    case SET_USER_AUTH: {
      return {
        ...state,
        ...action.payload
      }
    }
    default: {
      return state;
    }
  }
}

// Handle isRefreshing state
const isRefreshing = (state = null, action) => {
  switch(action.type) {
    case REFRESH_USER: {
      return action.payload.active;
    }
    default: {
      return false;
    }
  }
}

// Handle error state
const refreshError = (state = null, action) => {
  switch (action.type) {
    case REFRESH_USER_FAILURE: {
      return action.error;
    }
    default:
    case REFRESH_USER_SUCCESS: {
      return null;
    }
  }
}

export default combineReducers({
  loginError,
  refreshError,
  isLoggingIn,
  isRefreshing,
  user,
});