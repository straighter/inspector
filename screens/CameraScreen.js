import React, { Component } from 'react';
import { StyleSheet, PermissionsAndroid, TouchableOpacity } from 'react-native';
import { Container, View, Icon  } from 'native-base';
import { RNCamera } from 'react-native-camera';

import withLocation from '../hoc/withLocation';

import Camera from '../components/Camera';
import Map from '../components/Map';
import Box from '../components/Box';

// Camera screen handles taking a picture
// Camera screen has location awareness with a higher order component
@withLocation
class CameraScreen extends Component {
  state = {
    temporaryAssetURI: null
  }

  // Set flag for placeholder image for prototyping purposes in InspectionModal (set param for routes on screen load)
  componentDidMount() {
    this.props.navigation.setParams({
      usePlaceholderImage: this.usePlaceholderImage.bind(this)
    });
  }

  // Set flag for placeholder image for prototyping purposes in InspectionModal
  usePlaceholderImage() {
    this.props.navigation.navigate('NewInspection', { placeholder: true, location: this.props.currentLocation })
  }

  // Set picture data
  setPicture(data) {
    this.setState({
      temporaryAssetURI: data.uri,
      temporaryAssetPath: data.path
    });
  }

  // Take a picture if the camera module has loaded
  async takePicture() {
    if (this.camera) {
      const options = { fixOrientation: true, skipProcessing: true };

      try {
        const data = await this.camera.takePictureAsync(options)
        this.setPicture(data);
        this.handlePictureTaken();
      } catch(e) {
        console.log(e);
        return null;
      }      
    }
  }

  // Handle navigation after picture has been taken
  handlePictureTaken() {
    this.props.navigation.navigate('NewInspection', { ...this.state, location: this.props.currentLocation })
  }

  render() {
    return (
      <Container>
        <View style={{ flex: 4 }}>
          <Camera
            getRef={ref => { this.camera = ref }}
            onTakePicture={this.takePicture.bind(this)}
          />
        </View>
        <Map center={this.props.currentLocation} showUserLocation />
      </Container>
    );
  }
}

// Add navigation options, specifically adding a way to use a placeholder image in the header for prototype purposes in InspectionModal.
CameraScreen.navigationOptions = ({navigation}) => {
  return {
    title: 'Camera',
    headerTitle: 'Nieuw asset',
    headerRight: (
        <Box style={{ marginRight: 10 }} row>
            <TouchableOpacity onPress={() => navigation.getParam('usePlaceholderImage')()}><Icon name="image" /></TouchableOpacity>
            <TouchableOpacity style={{ marginLeft: 16 }} onPress={() => { navigation.navigate('Information')}}><Icon name='information-circle' /></TouchableOpacity>
        </Box>
    )
  }
}

export default CameraScreen;
