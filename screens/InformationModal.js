import React from 'react';
import { Footer, Container, Content, List, ListItem, Body, View, Text, Button, Icon, Left, Right } from 'native-base';
import { NavigationActions } from 'react-navigation';

import Box from '../components/Box';

// A simple information modal with extra information and a way to start the tutorial again.
export default class InformationModal extends React.Component {
  static navigationOptions = { title: 'Informatie' }

  render() {
    return (
      <Container>
        <Content>
          <List>
            <ListItem onPress={() => this.props.navigation.navigate('NENScreen')}>
              <Left>
                <Text>Beoordelen met de NEN-2767 normering</Text>
              </Left>
              <Right>
                <Icon name="arrow-forward" />
              </Right> 
            </ListItem>
          </List>
        </Content>
        <Footer style={{ backgroundColor: 'transparent' }}>
          <Button rounded success onPress={() => { 
            this.props.navigation.navigate('Introduction', { callback: () => { this.props.navigation.goBack(null) } }) 
          }}>
            <Text>Start de uitleg opnieuw</Text>
          </Button>
        </Footer>
      </Container>
    )
  }
}
