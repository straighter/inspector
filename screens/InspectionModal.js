import React from 'react';
import { connect } from 'react-redux';
import { TouchableOpacity, ActivityIndicator } from 'react-native';
import { bindActionCreators } from 'redux';
import { Container, Header, Title, View, DeckSwiper, Spinner, Card, CardItem, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';

import { getPrediction } from '../api/assets';

import { refresh } from '../actions/user';
import { fetchAssets, createAssetReview, createAsset } from '../actions/asset';
import withInspection from '../hoc/withInspection';
import InspectionWizard from '../components/InspectionWizard';
import idx from 'idx';

import Bubble from '../components/Bubble';
import Prediction from '../components/Prediction';
import Map from '../components/Map';
import Marker from '../components/Marker';
import Bar from '../components/Bar';
import Box from '../components/Box';
import ZoomableImage from '../components/ZoomableImage';

// Creates a new plain asset object
function assetFactory({author = null, prediction = null, reviews = [], location = null, uri = null}) {
    return {
        author,
        prediction, 
        reviews,
        location,
        uri
    }
}

// Screen used for inspection of a new asset
// Has inspection higher order component to handle inspection logic for reuse
@withInspection
@connect(({assets, auth}) => ({ 
    assets: assets.assets, 
    user: auth.user, 
    username: auth.user.username,
    userID: auth.user.id, 
    isFetching: assets.isFetching, 
    fetchingError: assets.fetchingError }), 
    dispatch => bindActionCreators({ fetchAssets, createAssetReview, createAsset }, dispatch)
)
class InspectionModal extends React.Component {
    state = {
        isFetchingPrediction: false,
        prediction: null
    }

    // On startup check if using placeholder for prototyping purposes
    constructor(props) {
        super(props);

        if (props.navigation.getParam('placeholder')) {
            this.currentAsset = assetFactory({
                author: props.userID,
                uri: 'https://i.imgur.com/fHlGOOU.jpg',
                location: props.navigation.state.params.location,
            });
        } else {
            this.currentAsset = assetFactory({
                author: props.userID,
                uri: props.navigation.state.params.temporaryAssetURI,
                location: props.navigation.state.params.location,
            });
        }
    }

    // Get a prediction for the current image and set a way to archive the asset from the header on screen load
    componentDidMount() {
        this.props.navigation.setParams({
            submitAsset: this.submitAsset.bind(this)
        });

        this.getPrediction();
    }

    // Fetch a prediction for the current image
    async getPrediction() {
        this.setState({
            isFetchingPrediction: true
        });

        const prediction = await getPrediction(this.currentAsset.uri, this.props.user.accessToken, this.props.user.refreshToken, refresh);

        this.setState({
            isFetchingPrediction: false,
            prediction
        })
    }
    
    // Navigate back to retake a picture
    retakePicture() {
        this.props.navigation.goBack();
    }

    // Archive the asset and create on server without review
    submitAsset() {
        try {
            this.props.createAsset(this.currentAsset)
            this.props.navigation.goBack();
        } catch (e) {
            console.log(e);
        }
    }

    // Create the asset with a review on server
    submitValue() {
        try {
            // Add review to asset
            this.currentAsset.reviews.push({
                author: this.props.userID,
                value: this.props.reviewValue
            });

            this.props.resetValue();
            this.props.createAsset(this.currentAsset)
            this.props.navigation.goBack();
        } catch (e) {
            console.log(e);
        }
    }

    render() {
        return (  
          <Container>
              <Box flex={3}>
                    <ZoomableImage source={{ uri: idx(this.currentAsset, _ => _.uri)}}>
                        <Bubble style={style.predictionOverlayRight}>
                            <Prediction isRobot>
                                {/*  Show indicator when loading or ??? when no prediction or error */}
                                {this.state.isFetchingPrediction ? <ActivityIndicator /> : (
                                    this.state.prediction ? (
                                            <Icon 
                                                name={this.state.prediction > 0.5 ? 'checkmark' : 'close'}
                                                style={{ 
                                                    fontSize: 20,
                                                    color: this.state.prediction > 0.5 ? '#5cb85c' : '#d9534f'
                                                }} 
                                            />
                                    ) : ( <Text>???</Text>)
                                )}
                            </Prediction> 
                        </Bubble>
                    </ZoomableImage>
            </Box>
            <Bar style={{ flex: 1 }}>
              <Box flex={2} padded>
              </Box>
              <Box flex={3} padded end>
                <Text>Gemaakt door</Text>
                <Text note>{this.props.username || "niet bekend"}</Text>
              </Box>
            </Bar>
            <Box flex={2}>
                <Map center={this.currentAsset.location}>
                    <Marker
                        title='Asset'
                        coordinate={this.currentAsset.location}
                    />
                </Map>
            </Box>
            <InspectionWizard reviewValue={this.props.reviewValue === null ? idx(this.currentAsset, _ => _.prediction.value) : this.props.reviewValue} setValue={this.props.setValue} submitValue={this.submitValue.bind(this)} />
          </Container>
        )
    }
}

// Set header action, in specific a way to archive the asset using submitAsset.
InspectionModal.navigationOptions = ({navigation}) => {
  return {
    title: 'Nieuw asset',
    headerRight: (
        <Box style={{ marginRight: 10 }} row>
            <TouchableOpacity onPress={() => navigation.getParam('submitAsset')()}><Icon name="archive" /></TouchableOpacity>
            <TouchableOpacity style={{ marginLeft: 16 }} onPress={() => { navigation.navigate('Information')}}><Icon name='information-circle' /></TouchableOpacity>
        </Box>
    )
  }
}

const style = {
    predictionOverlayRight: {
        position: 'absolute',
        bottom: 8,
        right: 8
    }
}

export default InspectionModal;
