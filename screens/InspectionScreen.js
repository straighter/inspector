import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { TouchableOpacity } from 'react-native'; 
import { Container, Header, Title, View, DeckSwiper, Spinner, Card, CardItem, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
import { fetchAssets, createAssetReview } from '../actions/asset';

import withInspection from '../hoc/withInspection';
import InspectionWizard from '../components/InspectionWizard';
import idx from 'idx';
import MapboxGL from '@mapbox/react-native-mapbox-gl';

import Map from '../components/Map';
import Marker from '../components/Marker';
import Bar from '../components/Bar';
import Box from '../components/Box';
import ZoomableImage from '../components/ZoomableImage';
import Bubble from '../components/Bubble';
import Prediction from '../components/Prediction';


// Screen to inspect existing assets
// Uses a withInspection higher order component to handle inspection logic in a reusable way
@withInspection
@connect(({assets, auth}) => ({ 
  assets: assets.assets, 
  userID: auth.user.id, 
  isFetching: assets.isFetching, 
  fetchingError: assets.fetchingError }), 
  dispatch => bindActionCreators({ fetchAssets, createAssetReview }, dispatch)
)
class InspectionScreen extends React.Component {
  state = {
    assetsIndex: 0,
  }

  // Get assets from server on screen load
  async componentDidMount() {
    await this.props.fetchAssets();
  }

  // Fetch assets from server and check if current asset has a review value
  async fetchAssets() {
    try {
      await this.props.fetchAssets();
      this.checkIfSetValue();
    } catch (e) {
    }
  }

  // Submit a review, reset the review value and go to the next asset
  async submitValue() {
    try {
      await this.props.createAssetReview(this.currentAsset().id, {
        author: this.props.userID,
        value: this.props.reviewValue
      });

      this.props.resetValue();
      this.reviewNext();
    } catch(e) {
      console.log(e);
    } 
  }
  
  // Go to the next asset
  reviewNext() {
    this.setState({
      assetsIndex: this.state.assetsIndex + 1
    }, this.checkIfSetValue);
  }

  // Check if a human review average is available and set it as default
  checkIfSetValue() {
    if (this.readyForReview()) {
      const humanReviewAverage = idx(this.currentAsset(), _ => Math.ceil(_.reviews.map(item => item.value).reduce((prev, next) => prev + next, 0) / _.reviews.length));
      this.props.setValue(humanReviewAverage || null);
    }
  }

  // Check if there are assets left to review
  readyForReview() {
    return this.props.assets.length > this.state.assetsIndex;
  }

  // Gets the current asset to review
  currentAsset() {
    return this.props.assets[this.state.assetsIndex]
  }

  render() {
    if (this.props.isFetching) {
      return (
        <Container>
          <Box flex={1} style={{ alignItems: 'center', justifyContent: 'center' }}>
            <Spinner />
          </Box>
        </Container>
      )
    } else if (!this.props.isFetching && this.props.fetchingError !== null) {
      return (
        <Container>
          <Box flex={1} style={{ alignItems: 'center', justifyContent: 'center' }}>
            <Text>Sorry, er is iets misgegaan! Probeer het nog eens.</Text>
            <Button success round onPress={this.fetchAssets.bind(this)}><Text>Opnieuw</Text></Button>
          </Box>
        </Container>
      )
    } else if (!this.readyForReview()) {
      return (
        <Container>
          <Box flex={1} style={{ alignItems: 'center', justifyContent: 'center' }}>
              <Text>Alle assets die u kon beoordelen zijn beoordeeld!</Text>
          </Box>
        </Container>
      )
    }
    
    const humanReviewAverage = idx(this.currentAsset(), _ => Math.ceil(_.reviews.map(item => item.value).reduce((prev, next) => prev + next, 0) / _.reviews.length));
    const robotReview = idx(this.currentAsset(), _ => _.prediction.value);

    // TODO: Fix weird marker update error
    const annotation =  <Marker
      title='Asset'
      coordinate={this.currentAsset().location}
    />
    
    return (  
      <Container>
        <Box flex={3}>
          <ZoomableImage source={{uri: idx(this.currentAsset(), _ => _.uri)}}>
            <Bubble style={style.predictionOverlayLeft}> 
              <Prediction>
                <Text style={{ fontSize: 16 }}>{humanReviewAverage || "???"}</Text>
              </Prediction>
            </Bubble>
            <Bubble style={style.predictionOverlayRight}> 
              <Prediction isRobot>
                {robotReview ? (
                    <Icon 
                    name={robotReview > 0.5 ? 'checkmark' : 'close'}
                    style={{ 
                        fontSize: 20,
                        color: robotReview > 0.5 ? '#5cb85c' : '#d9534f'
                    }} />
                  ) : ( <Text>???</Text>)
                } 
              </Prediction>            
            </Bubble>
          </ZoomableImage>
        </Box>
        <Bar style={{ flex: 1 }}>
          <Box flex={2} padded>
            <Text>Asset</Text>
            <Text note>{this.currentAsset().id}</Text>
          </Box>
          <Box flex={2} padded end>
            <Text>Gemaakt door</Text>
            <Text note>{idx(this.currentAsset(), _ => _.author.username) || "niet bekend"}</Text>
          </Box>
        </Bar>
        <Box flex={2}>
          <Map center={this.currentAsset().location}>
            {annotation}
          </Map>
        </Box>

        <InspectionWizard reviewValue={this.props.reviewValue} setValue={this.props.setValue} submitValue={this.submitValue.bind(this)} />
      </Container>
    )
  }
}

// Set header actions
InspectionScreen.navigationOptions = ({navigation}) => {
  return {
    title: 'Inspecteren',
    headerTitle: 'Bestaande assets',
    headerRight: (
      <TouchableOpacity onPress={() => { navigation.navigate('Information')}}>
        <Icon name='information-circle' style={{ paddingRight: 10 }}/>
      </TouchableOpacity>
    )
  }
}

const style = {
  emptyText: {
    color: '#080808',
    textAlign: 'center'
  },
  emptyContainer: {
    flex: 1,
    height: 500,
    justifyContent: 'center',
    alignItems: 'center'
  },
  predictionOverlayLeft: {
    position: 'absolute',
    bottom: 8,
    left: 8,
  },
  predictionOverlayRight: {
      position: 'absolute',
      bottom: 8,
      right: 8,
  },
}

export default InspectionScreen;
