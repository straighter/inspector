import React from 'react';
import { Container, Spinner } from 'native-base'
import { AsyncStorage } from 'react-native';

// Simple redirection screen that checks if it's the first time and redirects to the tutorial or app accordingly
class IntroductionRedirectionScreen extends React.Component {
  constructor(props) {
    super(props);
    this.bootstrapAuth();
  }
 
  async bootstrapAuth() {
    const firstTime = await AsyncStorage.getItem('firstTime');
    
    if (true) {
      this.props.navigation.navigate('Introduction', { callback: () => this.props.navigation.navigate('App') });
      await AsyncStorage.setItem('firstTime');
    } else {
      this.props.navigation.navigate('App')
    }
  }

  render() {
    return (
      <Container contentStyle={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Spinner />
      </Container>
    )
  }
}

export default IntroductionRedirectionScreen;
