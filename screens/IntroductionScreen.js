import React from 'react';
import { AsyncStorage } from 'react-native';
import { Container, Content, Text, Body,Button, Icon, List, ListItem } from 'native-base'
import Swiper from 'react-native-swiper';

import Title from '../components/Title';
import Caption from '../components/Caption';
import BodyText from '../components/BodyText';
import ZoomableImage from '../components/ZoomableImage';
import Bar from '../components/Bar';
import Box from '../components/Box';
import InspectionWizard from '../components/InspectionWizard';
import Map from '../components/Map';
import Marker from '../components/Marker';
import Bubble from '../components/Bubble';
import Prediction from '../components/Prediction';

import withInspection from '../hoc/withInspection';

// Manual navigation component for swiper.
const Navigation = ({index, total, context, callback}) => {
    if (index === 0) {
        return <Box full style={{ flex: 1, flexDirection: 'row', alignItems: 'center', position: 'absolute', bottom: 0, width: '100%', height: 60}}>
            <Button transparent danger style={{ marginRight: 'auto', marginLeft: 30 }} onPress={() => { callback() }}><Text>Overslaan</Text></Button>
            <Button info rounded style={{ marginLeft: 'auto', marginRight: 30 }} onPress={() => { context.current.scrollBy(1)}}><Icon name="arrow-forward"/></Button>
        </Box>
    } else if (index === total - 1) {
        return <Box full style={{ flex: 1, flexDirection: 'row', alignItems: 'center', position: 'absolute', bottom: 0, width: '100%', height: 60}}>
            <Button success rounded style={{ marginLeft: 'auto', marginRight: 30 }} onPress={() => callback()}><Icon name="checkmark"/></Button>
        </Box>
    }

    return <Box full style={{ flex: 1, flexDirection: 'row', alignItems: 'center', position: 'absolute', bottom: 0, width: '100%', height: 60}}>
        <Button light rounded style={{ marginRight: 'auto', marginLeft: 30 }} onPress={() => { context.current.scrollBy(-1)}}><Icon name="arrow-back"/></Button>
        <Button info rounded style={{ marginLeft: 'auto', marginRight: 30 }} onPress={() => { context.current.scrollBy(1)}}><Icon name="arrow-forward"/></Button>
    </Box>
}

// Tutorial screen with multiple swipable screens
// Uses withInspection higher order component to mock inspection interaction during tutorial
@withInspection
class IntroductionScreen extends React.Component {
    state = {
        index: 0
    }

    constructor() {
        super();

        this.swiper = React.createRef();
    }

    onChangeIndex(index) {
        this.setState({
            index
        })
    }

    render() {
        // Set static asset variables
        const coordinate = [52.3040189, 4.9039656];
        const id = '12x1dd92';
        const username = 'janvisser';
        const image = require('../images/1.jpg');

        return (
            <Container>
                <Swiper ref={this.swiper} onIndexChanged={this.onChangeIndex.bind(this)} loop={false}>
                    <Box padded>
                        <Title>Welkom in Inspector</Title>
                        <BodyText>In Inspector kunt u met gemak focussen op inspecteren, zonder bijzaken.</BodyText>
                        <BodyText>Inspector werkt simpel: inspecteren met maar een klik. Door het gemak en de kracht van het internet kunnen meerdere inspecteurs naar hetzelfde asset kijken en elkaar helpen. Ook kijkt en helpt de computer een handje mee!</BodyText>
                        <BodyText>In de volgende schermen leggen we u uit hoe het werkt en kunt u alles rustig uitproberen zonder gevolgen.</BodyText>
                    </Box>
                    <Box flex={1}>
                        <Box flex={2}>
                            <ZoomableImage source={image} />
                        </Box>
                        <Bar style={{ flex: 1 }}>
                            <Box flex={2} padded>
                                <Text>Asset</Text>
                                <Text note>ID: {id}</Text>
                            </Box>
                            <Box flex={3} padded end>
                                <Text>Gemaakt door</Text>
                                <Text note>{username}</Text>
                            </Box>
                        </Bar>
                        <Box flex={1}>
                            <Map center={coordinate}>
                                <Marker coordinate={coordinate} />
                            </Map>
                        </Box>
                        <Box flex={0.5} />
                        <Box padded flex={2}>
                            <Caption style={style.title}>Het inspecteren</Caption>
                            <BodyText>Tijdens het inspecteren is er een asset te zien. Informatie zoals een foto waarop in te zoomen is, de assetnaam en -soort, maar ook een interactieve kaart inclusief locatie is beschikbaar. Probeer het maar!</BodyText>
                        </Box>
                    </Box>
                    <Box flex={1}>
                        <Box flex={2}>
                            <ZoomableImage source={image} />
                        </Box>
                        <Bar style={{ flex: 1 }}>
                            <Box flex={2} padded>
                                <Text>Asset</Text>
                                <Text note>ID: {id}</Text>
                            </Box>
                            <Box flex={3} padded end>
                                <Text>Gemaakt door</Text>
                                <Text note>{username}</Text>
                            </Box>
                        </Bar>
                        <Box flex={1}>
                            <Map center={coordinate}>
                                <Marker coordinate={coordinate}/>
                            </Map>
                        </Box>
                        <Box flex={0.5}>
                            <InspectionWizard reviewValue={this.props.reviewValue} setValue={this.props.setValue} cancelValue={() => null} submitValue={() => null} />
                        </Box>
                        <Box padded flex={2}>
                            <Caption>Het beoordelen</Caption>
                            <BodyText>Na het inspecteren van het asset, kan u een beoordeling invoeren volgens de NEN-2767 norm. Druk maar eens op een nummer om een beoordeling te selecteren.</BodyText>
                            <BodyText>Hierna kunt u op het vinkje drukken om te beoordelen. Simpel!</BodyText>
                        </Box>
                    </Box>
                    <Box flex={1}>
                        <Box padded>
                            <Title>NEN-2767</Title>
                            <BodyText>De NEN-normering is een normering die gebruikt wordt om de kwaliteit aan te duiden van een asset. Hieronder staat uitgelegd wat de verschillende cijfers betekenen.</BodyText>
                        </Box>
                        <List>
                            <ListItem>
                            <Body>
                                <Text>1 &mdash; Uitstekend</Text>
                                <Text note>Geringe gebreken.</Text>
                            </Body>
                            </ListItem>
                            <ListItem>
                            <Body>
                                <Text>2 &mdash; Goed</Text>
                                <Text note>Beginnende veroudering.</Text>
                            </Body>
                            </ListItem>
                            <ListItem>
                            <Body>
                                <Text>3 &mdash; Redelijk</Text>
                                <Text note>Zichtbare veroudering, functievervulling niet in gevaar.</Text>
                            </Body>
                            </ListItem>
                            <ListItem>
                            <Body>
                                <Text>4 &mdash; Matig</Text>
                                <Text note>Zichtbare veroudering, functievervulling in gevaar.</Text>
                            </Body>
                            </ListItem>
                            <ListItem>
                            <Body>
                                <Text>5 &mdash; Slecht</Text>
                                <Text note>Veroudering onomkeerbaar.</Text>
                            </Body>
                            </ListItem>
                            <ListItem>
                            <Body>
                                <Text>6 &mdash; Zeer slecht</Text>
                                <Text note>Technisch rijp voor sloop.</Text>
                            </Body>
                            </ListItem>
                        </List>
                    </Box>
                    <Box flex={1}>
                        <Box flex={2}>
                            <ZoomableImage source={image}>
                                <Bubble style={style.predictionOverlayLeft}>
                                    <Prediction>
                                        <Text style={{ fontSize: 16 }}>1</Text>
                                    </Prediction>
                                </Bubble>
                                <Bubble style={style.predictionOverlayRight}>
                                    <Prediction isRobot>
                                        <Icon 
                                            name="checkmark" 
                                            style={{ fontSize: 20, color: '#5cb85c' }} 
                                        />
                                    </Prediction>
                                </Bubble>
                            </ZoomableImage>
                        </Box>
                        <Bar style={{ flex: 1 }}>
                            <Box flex={2} padded>
                                <Text>Asset</Text>
                                <Text note>ID: {id}</Text>
                            </Box>
                            <Box flex={3} padded end>
                                <Text>Gemaakt door</Text>
                                <Text note>{username}</Text>
                            </Box>
                        </Bar>
                        <Box flex={1}>
                            <Map center={coordinate}>
                                <Marker coordinate={coordinate}/>
                            </Map>
                        </Box>
                        <Box flex={0.5}>
                            <InspectionWizard reviewValue={2} setValue={() => null} cancelValue={() => null} submitValue={() => null} />
                        </Box>
                        <Box padded flex={2}>
                            <Caption>Beoordeelhulp</Caption>
                            <BodyText>De computer kijkt mee naar een asset en beoordeelt het asset ook. Met een vinkje of kruisje wordt aangeduid of het asset goed of slecht is.</BodyText>
                            <BodyText>Ook het gemiddelde van de beoordelingen van andere inspecteurs is te zien en wordt alvast ingevuld.</BodyText>
                        </Box>
                    </Box>
                    <Box padded flex={1}>
                        <Title>Nieuwe assets toevoegen</Title>
                        <BodyText>Naast het beoordelen van assets die al bestaan, kunnen er ook nieuwe assets worden toegevoegd door naar de camera te navigeren.</BodyText>
                        <BodyText>Eenmaal bij de camera kunt u een foto maken van het asset. Hierna kan u het beoordelen net zoals hiervoor.</BodyText>
                    </Box>
                </Swiper>
                <Navigation index={this.state.index} total={6} context={this.swiper} callback={this.props.navigation.getParam('callback')} />
            </Container>
        )
    }
}

const style = {
    predictionOverlayLeft: {
        position: 'absolute',
        bottom: 8,
        left: 8,
    },
    predictionOverlayRight: {
        position: 'absolute',
        bottom: 8,
        right: 8
    },
    mainTitle: {
        color: '#000000',
        fontSize: 32,
    },
    title: {
        color: '#000000',
        fontSize: 16,
        fontWeight: 'bold',
    },
}

export default IntroductionScreen;
