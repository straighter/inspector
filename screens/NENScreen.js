import React from 'react';
import { Header, Container, Content, List, ListItem, Body, View, Text, Button, Icon, Left, Right } from 'native-base';

import Box from '../components/Box';
import BodyText from '../components/BodyText';

// Information screen explaining the NEN-norm
export default class NENScreen extends React.Component {
	static navigationOptions = {
		title: 'NEN-2767'
	}

	render() {
		return (
			<Container>
				<Box padded>				
		  		<BodyText>De NEN-normering is een normering die gebruikt wordt om de kwaliteit aan te duiden van een asset. Hieronder staat uitgelegd wat de verschillende cijfers betekenen.</BodyText>
				</Box>
				<List>
					<ListItem>
						<Body>
						<Text>1 &mdash; Uitstekend</Text>
						<Text note>Geringe gebreken.</Text>
								</Body>
							</ListItem>
							<ListItem>
								<Body>
						<Text>2 &mdash; Goed</Text>
						<Text note>Beginnende veroudering.</Text>
								</Body>
							</ListItem>
							<ListItem>
								<Body>
						<Text>3 &mdash; Redelijk</Text>
						<Text note>Zichtbare veroudering, functievervulling niet in gevaar.</Text>
								</Body>
							</ListItem>
							<ListItem>
								<Body>
						<Text>4 &mdash; Matig</Text>
						<Text note>Zichtbare veroudering, functievervulling in gevaar.</Text>
								</Body>
							</ListItem>
							<ListItem>
								<Body>
						<Text>5 &mdash; Slecht</Text>
						<Text note>Veroudering onomkeerbaar.</Text>
								</Body>
							</ListItem>
							<ListItem>
								<Body>
						<Text>6 &mdash; Zeer slecht</Text>
						<Text note>Technisch rijp voor sloop.</Text>
								</Body>
							</ListItem>
						</List>
					</Container>
        )
    }
}
