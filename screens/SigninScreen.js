import React from 'react';
import { Alert } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Container, Content, Form, Title, Item, Spinner, Text, Input, Button, View } from 'native-base';

import { login } from '../actions/user';


// Screen that handles the login logic
@connect(({auth}) => ({ 
  isLoggingIn: auth.user.isLoggingIn,
  loginError: auth.user.loginError
}), dispatch => bindActionCreators({ login }, dispatch))
class SignIn extends React.Component {
  static navigationOptions = {
    title: 'Log in'
  }


  constructor() {
    super();

    this.state = {
      username: '',
      password: ''
    }
  }

  // Handle username change
  onChangeUsername(value) {
    this.setState({
      username: value
    });
  }

  // Handle password change
  onChangePassword(value) {
    this.setState({
      password: value
    });
  }

  // Request login
  async login() {
    try {
      await this.props.login(this.state.username, this.state.password);
      this.props.navigation.navigate('Authorized')
    } catch (e) {
      this.resetState();
    }
  }

  resetState() {
    this.setState({
      username: '',
      password: ''
    });
  }

  render() {
    const isDirtyInput = this.state.username !== '' || this.state.password !== '';
    const isLoggingIn = this.props.isLoggingIn;
    const loginError = this.props.loginError;
    
    return (
      <Container>
        <Content>
            <Form>
              <Item>
                <Input style={{ width: 80 }} placeholder='Gebruikersnaam' onChangeText={this.onChangeUsername.bind(this)} value={this.state.username} disabled={isLoggingIn}/>
              </Item>
              <Item last>
                <Input placeholder='Wachtwoord' onChangeText={this.onChangePassword.bind(this)} value={this.state.password} disabled={isLoggingIn} secureTextEntry />
              </Item>
            </Form>
            <Button info full onPress={this.login.bind(this)} disabled={isLoggingIn}>
              <Text>Log in</Text>
              {isLoggingIn && <Spinner color='#FFFFFF'/>}
            </Button>
            {loginError && <Text color='#d9534f'>Uw gebruikersnaam of wachtwoord is verkeerd.</Text>}
            <Text note style={{ textAlign: 'center', marginTop: 40 }}>Heeft u geen logingegevens? Neem dan contact op met uw leidinggevende om een account aan te vragen.</Text>
        </Content>
      </Container>
    );
  }
}

export default SignIn;
