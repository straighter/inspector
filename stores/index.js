import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger'
import { persistReducer, persistStore } from 'redux-persist' 
import storage from 'redux-persist/lib/storage';
import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2';
import rootReducer from '../reducers/rootReducer';

const logger = createLogger();

// Persist only the user key for auto-login, assets are async
const persistConfig = {
  key: 'root',
  storage: storage,
  stateReconciler: autoMergeLevel2,
  blacklist: ['assets']
}

const persistedReducer = persistReducer(persistConfig, rootReducer);


// Export persisted stores (persist data in SQLite)
export const store =  createStore(
  persistedReducer,
  applyMiddleware(
    thunk,
    logger
  )
);

export const persistor = persistStore(store);
